﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class CharSpeechEditor : EditorWindow
{
    public string characterName;

    public SpeechData speechData;

    [MenuItem("Window/Character Speech Editor")]
    static void Init()
    {
        EditorWindow.GetWindow(typeof(CharSpeechEditor)).Show();
    }

    private void OnGUI()
    {
        if (speechData != null)
        {
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("speechData");


            EditorGUILayout.LabelField(characterName, new GUIStyle { fontStyle = FontStyle.Bold });
            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Save data"))
            {
                SaveGameData();
            }

            
        }

        if (GUILayout.Button("Load data"))
        {
            LoadGameData();
        }

        if (GUILayout.Button("Create new data"))
        {
            CreateNewData();
        }
    }

    private void LoadGameData()
    {
        string filePath = EditorUtility.OpenFilePanel("Select data file", Application.streamingAssetsPath, "json");

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);

            speechData = JsonUtility.FromJson<SpeechData>(dataAsJson);
            string name = filePath.Substring(filePath.LastIndexOf("/")+1).Replace(".json", "");
            characterName = name;
        }
    }

    private void SaveGameData()
    {
        string filePath = EditorUtility.SaveFilePanel("Save data file", Application.streamingAssetsPath, "", "json");

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAsJson = JsonUtility.ToJson(speechData);
            dataAsJson = dataAsJson.Replace(",\"", ",\n  \"");
            dataAsJson = dataAsJson.Replace("{", "\n  {\n  ");
            File.WriteAllText(filePath, dataAsJson);
        }
    }

    private void CreateNewData()
    {
        speechData = new SpeechData();
    }

}