﻿using UnityEditor;

public class CreateAssetBundles : Editor
{
   [MenuItem("Assets/Build Asset Bundles")]
   public static void Build()
    {
        BuildPipeline.BuildAssetBundles("Assets/StreamingAssets/Bundles", BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets, BuildTarget.Android);
    }
}
