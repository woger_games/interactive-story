﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class BranchEditor : EditorWindow
{
    public AllBranches branchData;

    [MenuItem("Window/Branch Editor")]
    static void Init()
    {
        GetWindow(typeof(BranchEditor)).Show();
    }

    private void OnGUI()
    {
        if (branchData != null)
        {
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("branchData");
            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Save data"))
            {
                SaveGameData();
            }
        }

        if (GUILayout.Button("Load data"))
        {
            LoadGameData();
        }

        if (GUILayout.Button("Create new data"))
        {
            CreateNewData();
        }
    }

    private void LoadGameData()
    {
        string filePath = EditorUtility.OpenFilePanel("Select branch data file", Application.streamingAssetsPath, "json");

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);

            branchData = JsonUtility.FromJson<AllBranches>(dataAsJson);
        }
    }

    private void SaveGameData()
    {
        string filePath = EditorUtility.SaveFilePanel("Save branch data file", Application.streamingAssetsPath, "", "json");

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAsJson = JsonUtility.ToJson(branchData);
            dataAsJson = dataAsJson.Replace(",\"", ",\n  \"");
            dataAsJson = dataAsJson.Replace("{", "\n  {\n  ");
            File.WriteAllText(filePath, dataAsJson);
        }
    }

    private void CreateNewData()
    {
        branchData = new AllBranches();
    }
}
