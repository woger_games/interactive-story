﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class EnvironmentEditor : EditorWindow
{
    public EnvironmentData environmentData;

    [MenuItem("Window/Environment Editor")]
    static void Init()
    {
        GetWindow(typeof(EnvironmentEditor)).Show();
    }

    private void OnGUI()
    {
        if (environmentData != null)
        {
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("environmentData");
            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Save data"))
            {
                SaveGameData();
            }
        }

        if (GUILayout.Button("Load data"))
        {
            LoadGameData();
        }

        if (GUILayout.Button("Create new data"))
        {
            CreateNewData();
        }
    }

    private void LoadGameData()
    {
        string filePath = EditorUtility.OpenFilePanel("Select data file", Application.streamingAssetsPath, "json");

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);

            environmentData = JsonUtility.FromJson<EnvironmentData>(dataAsJson);
        }
    }

    private void SaveGameData()
    {
        string filePath = EditorUtility.SaveFilePanel("Save data file", Application.streamingAssetsPath, "", "json");

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAsJson = JsonUtility.ToJson(environmentData);
            dataAsJson = dataAsJson.Replace(",\"", ",\n  \"");
            dataAsJson = dataAsJson.Replace("{", "\n  {\n  ");
            File.WriteAllText(filePath, dataAsJson);
        }
    }

    private void CreateNewData()
    {
        environmentData = new EnvironmentData();
    }
}
