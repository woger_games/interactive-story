﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogUI : MonoBehaviour
{
    public float minDuration = 2.8f;
    
    [Space(5)]
    [SerializeField] TextMeshProUGUI txtCharacterName;
    [SerializeField] TextMeshProUGUI txtPlayerName;
    [SerializeField] Transform       branchGroup;
    [SerializeField] GameObject      branchUiPrefab;
    [SerializeField] SpeechPanel     playerPanel;
    [SerializeField] SpeechPanel     narratorPanel;
    [SerializeField] SpeechPanel     characterPanel;
    

    public delegate void EndSpeech(NextSpeechData nextSpeech);
    public EndSpeech endSpeechCallback;


    public void OnClick()
    {

    }

    public void UpdateUiSpeech(string name, string text, NextSpeechData nextSpeech)
    {
        StartCoroutine(ClearBranchesUI());

        ShowSpeechPanel(name);
        SetPanelText(name, text);
        SetPanelName(name);

        if(nextSpeech.characterName == "branch")
        {
            BranchData branchData = SpeechManager.GetBranchData(nextSpeech.nextSpeechKey);
            foreach(Branch branch in branchData.items)
            {
                BranchUI b = Instantiate(branchUiPrefab, branchGroup).GetComponent<BranchUI>();
                b.SetBranch(branch);
            }
        }
        else
        {
            float duration = CalculateDurationByTextLength(text);
            StartCoroutine(ShowTimer(duration, nextSpeech));
        }

    }

    void ShowSpeechPanel(string name)
    {
        HideAllpanel();
        string panelType = GetPanelTypeByName(name);
        GetSpeechPanel(panelType).gameObject.SetActive(true);
    }

    void HideAllpanel()
    {
        playerPanel.gameObject.SetActive(false);
        narratorPanel.gameObject.SetActive(false);
        characterPanel.gameObject.SetActive(false);
    }

    string GetPanelTypeByName(string name)
    {
        return name == SpeechManager.playerName ? "Player" : name == "Narrator" ? name : "Character";
    }

    void SetPanelText(string name, string text)
    {
        string panelType = GetPanelTypeByName(name);
        GetSpeechPanel(panelType).txtSpeech.text = text;
        
    }

    SpeechPanel GetSpeechPanel(string type)
    {
        switch (type)
        {
            case "Player":
                {
                    return playerPanel;
                }
            case "Character":
                {
                    return characterPanel;
                }
            case "Narrator":
                {
                    return narratorPanel;
                }
        }
        return null;
    }

    void SetPanelName(string name)
    {
        if (name == "Narrator")
            return;
        if (name == SpeechManager.playerName)
        {
            txtPlayerName.text = name;
        }
        else
        {
            txtCharacterName.text = name;
        }

    }

    IEnumerator ClearBranchesUI()
    {
        int length = branchGroup.childCount;

        for (int i = 0; i < length; i++)
        {
            Destroy(branchGroup.GetChild(0).gameObject);
            yield return null;
        }        
    }

    float CalculateDurationByTextLength(string text)
    {
        return Mathf.Clamp(text.Length / 15f, minDuration, 15);
    }

    IEnumerator ShowTimer(float duration, NextSpeechData nextSpeech)
    {
        yield return new WaitForSeconds(duration);
        endSpeechCallback.Invoke(nextSpeech);
    }
}
