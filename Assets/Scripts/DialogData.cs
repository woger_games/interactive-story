﻿
#region SPEECH
[System.Serializable]
public class SpeechData
{
    public Speech[] items;
}

[System.Serializable]
public class Speech
{
    public string key;
    [UnityEngine.TextArea]
    public string text;
    public NextSpeechData nextSpeech;
}

[System.Serializable]
public class NextSpeechData
{
    public string characterName;
    public string nextSpeechKey;
}
#endregion

#region BRANCH
[System.Serializable]
public class AllBranches
{
    public BranchData[] branches;
}

[System.Serializable]
public class BranchData
{
    public string speechKey;
    public Branch[] items;
}

[System.Serializable]
public class Branch
{
    public string nextSpeechKey;
    public string branchDescription;
}
#endregion

#region ENVIRONMENT
[System.Serializable]
public class EnvironmentData
{
    public Environment[] items;
}

[System.Serializable]
public class Environment
{
    public string speechKey;
    public string spriteName;
} 
#endregion
