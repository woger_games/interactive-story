﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentManager : MonoBehaviour
{
    [SerializeField] Sprite[] sprites;

    Environment[] environments;

    void Awake()
    {
        environments = LoadManager.LoadEnvironmenData().items;
        sprites = LoadManager.LoadSprites("backgrounds").ToArray();
    }

    public void CheckReplace(string speechKey)
    {
        foreach(Environment environment in environments)
        {
            if(environment.speechKey == speechKey)
            {
                SwitchSprite(environment.spriteName);
            }
        }
    }

    void SwitchSprite(string spriteName)
    {
        foreach (Sprite sprite in sprites)
        {
            string name = sprite.name;
            if (name == spriteName)
            {
                GetComponent<SpriteRenderer>().sprite = sprite;
            }
            
        }
    }
}
