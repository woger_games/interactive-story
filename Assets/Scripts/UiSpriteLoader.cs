﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiSpriteLoader : MonoBehaviour
{
    [SerializeField] string fileName;

    void Start()
    {
        foreach (var item in LoadManager.LoadSprites("ui"))
        {
            if(item.name == fileName)
            {
                GetComponent<Image>().sprite = item;
                break;
            }
        }
        Destroy(this);
    }

}
