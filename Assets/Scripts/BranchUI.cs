﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BranchUI : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI txtDescription;

    Branch branch;

    public void SetBranch(Branch branch)
    {
        this.branch = branch;
        txtDescription.text = branch.branchDescription;
    }

    public void OnClick()
    {
        NextSpeechData nextSpeech = new NextSpeechData();
        nextSpeech.characterName = SpeechManager.playerName;
        nextSpeech.nextSpeechKey = branch.nextSpeechKey;
        SpeechManager.FindNextSpeech(nextSpeech);
    }


}
