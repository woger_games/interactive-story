﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpeechManager : MonoBehaviour
{
    [SerializeField] DialogUI dialog;
    [SerializeField] EnvironmentManager environmentManager;


    static BranchData[] branches;
    public static string playerName = "Kate";
    public static List<Character> characters = new List<Character>();

    public static EnvironmentManager environment;

    void Awake()
    {
        characters.Clear();
    }

    void Start()
    {   
        branches = LoadManager.LoadBranchData().branches;
        environment = environmentManager;

        StartStory();
    }

    void StartStory()
    {
        NextSpeechData nsd = new NextSpeechData();
        nsd.characterName = "Narrator";
        nsd.nextSpeechKey = "begin";
        FindNextSpeech(nsd);
    }

    static void OnSpeechEnd(NextSpeechData nextSpeech)
    {
        HideCharacters();
        FindNextSpeech(nextSpeech);
        
    }

    public static void FindNextSpeech(NextSpeechData nextSpeech)
    {
        foreach (Character character in characters)
        {
            if(character.name == nextSpeech.characterName)
            {
                environment.CheckReplace(nextSpeech.nextSpeechKey);
                character.StartSpeech(nextSpeech.nextSpeechKey);
            }
        }
    }

    static void HideCharacters()
    {
        foreach (Character character in characters)
        {
            Vector3 pos = character.SpeechPosition;
            pos.x *= 5;
            character.MoveToPosition(pos);
        }
    }

    public static BranchData GetBranchData(string key)
    {
        foreach(BranchData branchData in branches)
        {
            if (branchData.speechKey == key) return branchData;
        }
        return null;
    }

    public static void AddCharacter(Character character)
    {
        characters.Add(character);
    }

    private void OnEnable()
    {
        dialog.endSpeechCallback += OnSpeechEnd;
    }

    private void OnDisable()
    {
        dialog.endSpeechCallback -= OnSpeechEnd;
    }

    public void OnClickReplay()
    {
        SceneManager.LoadScene(0);
    }
}
