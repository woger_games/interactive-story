﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class LoadManager : MonoBehaviour
{
    public int bundleVersion = 4;

    static Dictionary<string, List<Sprite>> loadedSprites = new Dictionary<string, List<Sprite>>();

    void Start()
    {
        StartCoroutine(LoadAssets());
    }

    public static SpeechData LoadSpeech(string characterName)
    {
        SpeechData speechData = new SpeechData();

        string fileName = characterName + ".json";

        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        WWW www = new WWW(filePath);
        while (!www.isDone) { }
        
        string dataAsJson = www.text;
        speechData = JsonUtility.FromJson<SpeechData>(dataAsJson);
        

        return speechData;
    }

    public static AllBranches LoadBranchData()
    {
        AllBranches speechData = new AllBranches();

        string fileName =  "BranchData.json";

        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        WWW www = new WWW(filePath);
        while (!www.isDone) { }

        string dataAsJson = www.text;
        speechData = JsonUtility.FromJson<AllBranches>(dataAsJson);
        

        return speechData;
    }

    public static EnvironmentData LoadEnvironmenData()
    {
        EnvironmentData environmentData = new EnvironmentData();

        string fileName = "EnvironmentData.json";

        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        WWW www = new WWW(filePath);
        while (!www.isDone) { }

        string dataAsJson = www.text;
        environmentData = JsonUtility.FromJson<EnvironmentData>(dataAsJson);
        

        return environmentData;
    }


    public static List<Sprite> LoadSprites(string folderName)
    {
        folderName = folderName.ToLower();
        if (loadedSprites.ContainsKey(folderName))
        {
            return loadedSprites[folderName];
        }
        return new List<Sprite>();
    }

    public IEnumerator LoadAssets()
    {
        Caching.ClearCache();
        while (!Caching.ready)
        {
            print("шо це тако");
            yield return null;
        }
        
        string path = Path.Combine(Application.streamingAssetsPath, "Bundles/sprites");

        var request = UnityWebRequestAssetBundle.GetAssetBundle(path);

        var async = request.SendWebRequest();
        yield return async;

        var assetBundle = ((DownloadHandlerAssetBundle)request.downloadHandler).assetBundle;

        foreach (var s in assetBundle.GetAllAssetNames())
        {
            string str = "sprites/";
            int startIndex = s.IndexOf(str) + str.Length;
            str = s.Substring(startIndex);
            string key = str.Substring(0, str.IndexOf("/"));

            string fileName = s.Substring(s.LastIndexOf("/")+1);

            var spriteRequest = assetBundle.LoadAssetAsync(s, typeof(Sprite));

            yield return spriteRequest;

            if (loadedSprites.ContainsKey(key))
            {
                loadedSprites[key].Add((Sprite)spriteRequest.asset);
            }
            else
            {
                List<Sprite> sprites = new List<Sprite>();
                sprites.Add((Sprite)spriteRequest.asset);
                loadedSprites.Add(key, sprites);
            }
        }
        assetBundle.Unload(false);
        
        SceneManager.LoadScene(1);
    }
}
