﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    new public string name;

    [SerializeField] Vector3 characterPosition;
    [Header("References")]
    [SerializeField] SpriteRenderer hair;
    [SerializeField] SpriteRenderer face;
    [SerializeField] SpriteRenderer body;
    [SerializeField] SpriteRenderer emotion;

    [Header("Load Settings")]
    [SerializeField] string folderName;
    [SerializeField] string bodyName;

    public List<Sprite> kbkkv;

    SpeechData speechData;
    DialogUI dialogUI;
    
    public Vector3 SpeechPosition
    {
        get { return characterPosition; }
    }

    void Start()
    {
        speechData = LoadManager.LoadSpeech(name);
        dialogUI = FindObjectOfType<DialogUI>();

        SpeechManager.AddCharacter(this);

        LoadSprites();
    }

    public void StartSpeech(string key)
    {
        Speech speech = GetSpeechByKey(key);
        
        dialogUI.UpdateUiSpeech(name, speech.text, speech.nextSpeech);
        MoveToPosition(characterPosition);
        
    }

    public void MoveToPosition(Vector3 pos)
    {
        StopAllCoroutines();
        if (name == SpeechManager.playerName)
        {
            pos.x *= -1;
        }
        StartCoroutine(Move(pos));
    }

    IEnumerator Move(Vector3 pos)
    {
        Vector2 velocity = Vector2.zero;
        while(Vector2.Distance(transform.position, pos) > 0.01f)
        {
            transform.position = Vector2.SmoothDamp(transform.position, pos, ref velocity, 0.19f);
            yield return null;
        }
        transform.position = pos;
    }

    Speech GetSpeechByKey(string key)
    {
        foreach(Speech s in speechData.items)
        {
            if (s.key == key) return s;
        }
        return null;
    }

    void LoadSprites()
    {
        kbkkv = LoadManager.LoadSprites(folderName);
        string name;
        foreach (Sprite sprite in LoadManager.LoadSprites(folderName))
        {
            name = sprite.name;
            if (name.IndexOf("hair") > -1)
            {
                hair.sprite = sprite;
            }
            if(name.IndexOf("neutral") > -1)
            {
                face.sprite = sprite;
            }
            if(name.IndexOf(bodyName) > -1)
            {
                body.sprite = sprite;
            }
        }
        if (this.name != "Narrator")
        {
            foreach (Sprite sprite in LoadManager.LoadSprites("emotion backgrounds"))
            {
                name = sprite.name;
                if (name.IndexOf("Neutral") > -1)
                {
                    emotion.sprite = sprite;
                }
            }
        }
    }

}
